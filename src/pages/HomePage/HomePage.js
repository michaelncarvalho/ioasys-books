import React, { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { isEmpty } from 'lodash'
import uuid from 'react-uuid'
import { Layout, Button, Notification, Loading } from 'element-react'
import { getFirstWorld } from '../../utils/helpers/String'
import AuthContext from '../../store/auth-context'
import { book } from '../../services'
import { store } from '../../store'
import { setBooks } from '../../store/actions/bookActions'
import logoIoasys from '../../assets/logo-ioasys-black.svg'
import BookItem from '../../components/BookItem'
import Pagination from '../../components/Pagination'
import BookModal from '../../components/BookModal'
import './HomePage.scss'

const PAGE_SIZE = 6
const INITIAL_PAGE = 1
const CATEGORY = 'biographies'

const HomePage = () => {
  const authCtx = useContext(AuthContext)
  const history = useHistory()
  const [page, setPage] = useState(INITIAL_PAGE)
  const [pageSize, setPageSize] = useState(PAGE_SIZE)
  const [loading, setLoading] = useState(false)
  const [totalItems, setTotalItems] = useState(0)
  const [books, setBooksList] = useState([])
  const [bookDialogVisibility, setBookDialogVisibility] = useState(false)
  const [bookData, setBookData] = useState({})

  const handleLogoutClick = () => {
    authCtx.logout()
    history.replace('/ioasys-books/login')
  }

  const getBooks = () => {
    setBooksList([])
    setLoading(true)
    book
      .getBooks(page, pageSize, CATEGORY)
      .then((response) => {
        store.dispatch(setBooks(response.data.data))
        setTotalItems(response.data.totalItems)
        setBooksList(response.data.data)
      })
      .catch(() => {
        Notification.error({
          title: 'Erro',
          message: 'Ocorreu um erro ao buscar a lista de livros.',
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }

  const getBookById = (idBook) => {
    setLoading(true)
    book
      .getBookById(idBook)
      .then((response) => {
        setBookData(response.data)
        setBookDialogVisibility(true)
      })
      .catch(() => {
        Notification.error({
          title: 'Erro',
          message: 'Ocorreu um erro ao buscar as informações do livro.',
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }

  useEffect(() => {
    setPage(INITIAL_PAGE)
    setPageSize(PAGE_SIZE)
    getBooks()
  }, [])

  useEffect(() => {
    getBooks()
  }, [page])

  return (
    <Loading text='Carregando os dados...' loading={loading}>
      <Layout.Row type='flex' className='home-container'>
        <Layout.Row type='flex' align='middle' className='home-header'>
          <Layout.Col className='logo-home-container'>
            <img
              className='logo-ioasys'
              alt='Imagem com o logo da Ioasys'
              src={logoIoasys}
            />
            <span className='logo-title-home'>Books</span>
          </Layout.Col>
          <Layout.Col className='logout-container'>
            <div className='welcome-sentence'>
              Bem vindo,{' '}
              <span className='user-name'>
                {getFirstWorld(authCtx.user?.name)}
              </span>
              !
            </div>
            <Button
              plain
              type='primary'
              size='small'
              className='logout-button'
              onClick={() => handleLogoutClick()}
            >
              <i className='mdi mdi-logout' />
            </Button>
          </Layout.Col>
        </Layout.Row>
        <Layout.Row type='flex' className='books-box'>
          {books &&
            books.length > 0 &&
            books.map((a, index) => (
              <BookItem
                key={uuid()}
                id={index}
                idBook={a.id}
                title={a.title}
                authors={a.authors}
                imageUrl={a.imageUrl}
                published={a.published}
                publisher={a.publisher}
                pageCount={a.pageCount}
                onClick={(value) => getBookById(value)}
              />
            ))}
        </Layout.Row>
        {!isEmpty(bookData) && (
          <BookModal
            bookData={bookData}
            dialogVisible={bookDialogVisibility}
            onCancel={() => setBookDialogVisibility(false)}
          />
        )}
        <Layout.Row className='pagination-row'>
          <Pagination
            page={page}
            pageSize={pageSize}
            totalItems={totalItems}
            setPage={(value) => setPage(value)}
          />
        </Layout.Row>
      </Layout.Row>
    </Loading>
  )
}

export default HomePage
