import React, { useState } from 'react'
import { Layout, Loading } from 'element-react'
import AuthForm from '../../components/AuthForm'

import logoIoasys from '../../assets/logo-ioasys.svg'
import 'element-theme-default'
import './Login.scss'

const LoginPage = () => {
  const [loading, setLoading] = useState(false)

  return (
    <Loading loading={loading} text='Autenticando o usuário e senha'>
      <div className='login-container'>
        <div className='login-content'>
          <Layout.Row type='flex' align='middle' className='logo-row'>
            <img
              className='logo-ioasys'
              alt='Imagem com o logo da Ioasys'
              src={logoIoasys}
            />
            <span className='logo-title'>Books</span>
          </Layout.Row>
          <AuthForm setLoading={setLoading} />
        </div>
      </div>
    </Loading>
  )
}

export default LoginPage
