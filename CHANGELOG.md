# Changelog

Todas as mudanças importantes deste projeto serão documentadas nesse arquivo.

Formato de CHANGELOG baseado em [Keep a Changelog](https://keepachangelog.com/pt-BR/1.0.0/) seguindo o modelo de [versionamento semântico](https://semver.org/lang/pt-BR/).

## [Unreleased] - 14/06/2021

### Adicionado
- interface login
- lógica persistência login
- interface home page
- integração serviços API
