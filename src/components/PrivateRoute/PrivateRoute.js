import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import AuthContext from '../../store/auth-context'

const PrivateRoute = ({ component: Component, ...rest }) => {
  const authCtx = useContext(AuthContext)

  return (
    <Route
      rest={rest}
      render={(props) =>
        authCtx.isLoggedIn ? (
          <Component props={props} />
        ) : (
          <Redirect to='/ioasys-books/login' />
        )
      }
    />
  )
}

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
}

export default PrivateRoute
