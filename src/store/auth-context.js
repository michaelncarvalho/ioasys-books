import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'

const AuthContext = React.createContext({
  token: '',
  isLoggedIn: false,
  user: {},
  login: () => {},
  logout: () => {},
})

const retrieveStoredToken = () => {
  const storedToken = localStorage.getItem('token')

  return {
    token: storedToken,
  }
}

const retrieveStoredUser = () => {
  const storedUser = localStorage.getItem('user')

  return JSON.parse(storedUser)
}

export const AuthContextProvider = (props) => {
  const tokenData = retrieveStoredToken()
  const userData = retrieveStoredUser()
  let initialToken
  if (tokenData) {
    initialToken = tokenData.token
  }

  const [token, setToken] = useState(initialToken)
  const [user, setUser] = useState(userData || {})
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(!!token)

  const logoutHandler = useCallback(() => {
    setToken(null)
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    setUserIsLoggedIn(false)
  }, [])

  const loginHandler = (newToken, newUser) => {
    setToken(newToken)
    setUser(newUser)
    localStorage.setItem('token', newToken)
    localStorage.setItem('user', JSON.stringify(newUser))
    setUserIsLoggedIn(true)
  }

  const contextValue = {
    token,
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
    user,
  }

  const { children } = props

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  )
}

AuthContextProvider.propTypes = {
  children: PropTypes.element,
}

AuthContextProvider.defaultProps = {
  children: null,
}

export default AuthContext
