import { combineReducers } from 'redux'
import { bookReducer } from './bookReducer'

function lastAction(action = null) {
  return action
}

export const rootReducer = combineReducers({
  bookState: bookReducer,
  lastAction,
})
