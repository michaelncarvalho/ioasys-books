import React from 'react'
import Router from './router'
import './styles/main.scss'

function App() {
  return <Router />
}

export default App
