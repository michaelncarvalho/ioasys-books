import http from './http'

export default {
  login(payload) {
    return http.post(`auth/sign-in`, payload)
  },
}
