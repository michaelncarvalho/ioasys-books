import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'element-react'
import { useHistory } from 'react-router-dom'

import AuthContext from '../../store/auth-context'
import { auth } from '../../services'
import TextField from '../TextField'
import DialogCard from '../DialogCard'

import './AuthForm.scss'

const AuthForm = (props) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const history = useHistory()
  const [showDialog, setShowDialog] = useState(false)

  const authCtx = useContext(AuthContext)

  const handleLogin = () => {
    props.setLoading(true)
    auth
      .login({ email, password })
      .then((response) => {
        authCtx.login(response.headers.authorization, response.data)
        history.replace('/ioasys-books/home')
      })
      .catch(() => {
        setShowDialog(true)
      })
      .finally(() => {
        props.setLoading(false)
      })
  }

  useEffect(() => {
    authCtx.logout()
  }, [])

  return (
    <div className='login-form'>
      <TextField
        type='email'
        label='Email'
        value={email}
        onChange={(event) => {
          setShowDialog(false)
          setEmail(event.target.value)
        }}
      />
      <TextField
        label='Senha'
        value={password}
        type='password'
        onChange={(event) => {
          setShowDialog(false)
          setPassword(event.target.value)
        }}
        button={
          <Button
            plain
            type='primary'
            className='submit-button'
            onClick={() => handleLogin()}
          >
            Entrar
          </Button>
        }
      />
      <DialogCard
        dialogVisible={showDialog}
        text='Email e/ou senha incorretos.'
      />
    </div>
  )
}

AuthForm.propTypes = {
  setLoading: PropTypes.func.isRequired,
}

export default AuthForm
