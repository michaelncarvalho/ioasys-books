import React from 'react'
import PropTypes from 'prop-types'

import './DialogCard.scss'

const DialogCard = ({ text, customStyle, dialogVisible }) =>
  dialogVisible && (
    <div className='dialog-card' style={customStyle}>
      <div className='dialog-content'>{text}</div>
    </div>
  )

DialogCard.propTypes = {
  text: PropTypes.string,
  customStyle: PropTypes.element,
  dialogVisible: PropTypes.bool,
}

DialogCard.defaultProps = {
  text: '',
  customStyle: null,
  dialogVisible: false,
}

export default DialogCard
