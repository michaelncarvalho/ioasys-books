import { SET_BOOKS_DATA, RESET_STATE } from '../actionTypes'

const INITIAL_STATE = {
  books: { name: 'teste' },
}

export const bookReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RESET_STATE:
      return {
        ...INITIAL_STATE,
      }
    case SET_BOOKS_DATA:
      return {
        ...state,
        books: action.books,
      }
    default:
      return state
  }
}
