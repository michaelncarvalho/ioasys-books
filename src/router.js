import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import PrivateRoute from './components/PrivateRoute'
import PublicRoute from './components/PublicRoute'
import LoginPage from './pages/LoginPage'
import HomePage from './pages/HomePage'

const Router = () => (
  <BrowserRouter>
    <Switch>
      <PublicRoute exact path='/ioasys-books/login' component={LoginPage} />
      <PrivateRoute path='/ioasys-books/home' component={HomePage} />

      <PublicRoute exact path='/' component={LoginPage} />
    </Switch>
  </BrowserRouter>
)

export default Router
