import axios from 'axios'
import { get } from 'lodash'
import qs from 'qs'

const StatusCodesHandlers = {
  401: () => {
    window.location.href = process.env.REACT_APP_HTTP_URL
    localStorage.clear()
  },
}

const handleError = (error) => {
  const handle = StatusCodesHandlers[get(error, 'response.status')]
  if (handle) handle.call(null, error)
  return Promise.reject(error)
}

const http = axios.create({
  baseURL: 'https://books.ioasys.com.br/api/v1/',
  responseType: 'json',
  paramsSerializer: (params) => qs.stringify(params),
})

http.interceptors.response.use(
  (response) => response,
  (error) => handleError(error)
)

http.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`
  return config
})

export default http
