import http from './http'

export default {
  getBooks(page, amount, category) {
    return http.get(`books`, {
      params: {
        page,
        amount,
        category,
      },
    })
  },
  getBookById(idBook) {
    return http.get(`books/${idBook}`)
  },
}
