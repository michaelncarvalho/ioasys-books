# Ioasys Book App

> Para o controle de versões ver o [CHANGELOG](./CHANGELOG.md)

## Quick start

### `yarn start`

Executa o aplicativo no modo de desenvolvimento. 

Abra [http://localhost: 3007](http://localhost: 3007) para visualizá-lo no navegador.

A página será recarregada se você fizer edições. 

Você também verá quaisquer erros de lint no console.

### `yarn test`

Inicia o executor de teste no modo de observação interativo.

### `yarn build`

Compila o aplicativo para produção na pasta `build`. 
Ele agrupa corretamente o React no modo de produção e otimiza a construção para o melhor desempenho.

### `yarn format`

Executa o formatador de código configurado na aplicação. Nessa aplicação foi utilizado o Prettier.

