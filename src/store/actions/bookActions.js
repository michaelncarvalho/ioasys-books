import { SET_BOOKS_DATA, RESET_STATE } from '../actionTypes'

export const resetState = () => ({ type: RESET_STATE })

export const setBooks = (books) => ({
  type: SET_BOOKS_DATA,
  books,
})
