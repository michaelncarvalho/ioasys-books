import { render, screen } from '@testing-library/react'
import App from './App'

test('Procura pelo título Books na página de login', () => {
  render(<App />)
  const title = screen.getByText('Books')
  expect(title).toBeInTheDocument()
})
