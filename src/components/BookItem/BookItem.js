import React from 'react'
import PropTypes from 'prop-types'
import { Layout } from 'element-react'
import uuid from 'react-uuid'
import defaultImage from '../../assets/default-book-image.png'
import './BookItem.scss'

const BookItem = ({
  index,
  idBook,
  title,
  authors,
  pageCount,
  publisher,
  imageUrl,
  published,
  customStyle,
  maxAuthors,
  onClick,
}) => (
  <div
    tabIndex={index}
    role='button'
    className='book-card'
    style={customStyle}
    onKeyDown={() => onClick(idBook)}
    onClick={() => onClick(idBook)}
  >
    <Layout.Row type='flex' className='book-content'>
      <Layout.Col className='book-image-container'>
        <img
          src={imageUrl || defaultImage}
          alt='Imagem com a capa do livro'
          className='book-image'
        />
      </Layout.Col>
      <Layout.Col className='book-description-col'>
        <Layout.Row>
          <Layout.Row className='book-title'>{title}</Layout.Row>
          {authors.slice(0, maxAuthors).map((author) => (
            <Layout.Row key={uuid()} className='book-author'>
              {author}
            </Layout.Row>
          ))}
        </Layout.Row>
        <Layout.Row>
          <Layout.Row className='subtitles-books'>
            {pageCount} páginas
          </Layout.Row>
          <Layout.Row className='subtitles-books'>
            Editora {publisher.split(' ').slice(0, 1)}
          </Layout.Row>
          <Layout.Row className='subtitles-books'>
            Publicado em {published}
          </Layout.Row>
        </Layout.Row>
      </Layout.Col>
    </Layout.Row>
  </div>
)

BookItem.propTypes = {
  customStyle: PropTypes.element,
  title: PropTypes.string,
  authors: PropTypes.arrayOf(PropTypes.string),
  pageCount: PropTypes.number,
  publisher: PropTypes.string,
  imageUrl: PropTypes.string,
  published: PropTypes.number,
  maxAuthors: PropTypes.number,
  onClick: PropTypes.func,
  idBook: PropTypes.string,
  index: PropTypes.number,
}

BookItem.defaultProps = {
  customStyle: null,
  idBook: '',
  title: 'Título do Livro',
  authors: [],
  pageCount: 0,
  publisher: 'Editora',
  imageUrl: '',
  published: 0,
  maxAuthors: 2,
  onClick: () => {},
  index: 1,
}

export default BookItem
