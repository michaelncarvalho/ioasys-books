import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'element-react'
import './Pagination.scss'

const Pagination = ({ page, pageSize, totalItems, setPage }) => {
  const getTotalPages = () => Math.ceil(totalItems / pageSize)

  const onRightClick = () =>
    page + 1 <= getTotalPages() ? setPage(page + 1) : setPage(page)

  const onLeftClick = () => (page - 1 >= 1 ? setPage(page - 1) : setPage(page))

  return (
    <div className='pagination-container'>
      <div className='pagination-text'>
        Página {page} de {getTotalPages()}
      </div>
      <Button
        plain
        type='primary'
        size='small'
        className='pagination-button'
        disabled={page === 1}
        onClick={() => onLeftClick()}
      >
        <i className='mdi mdi-chevron-left icon-button' />
      </Button>
      <Button
        plain
        type='primary'
        size='small'
        className='pagination-button'
        disabled={page === getTotalPages()}
        onClick={() => onRightClick()}
      >
        <i className='mdi mdi-chevron-right icon-button' />
      </Button>
    </div>
  )
}

Pagination.propTypes = {
  page: PropTypes.number,
  totalItems: PropTypes.number,
  pageSize: PropTypes.number,
  setPage: PropTypes.func,
}

Pagination.defaultProps = {
  page: 1,
  totalItems: 0,
  pageSize: 0,
  setPage: () => {},
}

export default Pagination
