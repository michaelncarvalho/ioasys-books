import React from 'react'
import PropTypes from 'prop-types'

import './TextField.scss'

const TextField = ({ value, label, onChange, type, button }) => (
  <div className='text-field'>
    <div className='label-text-field'>{label}</div>
    <div className='value-container'>
      <input
        className='value-text-field'
        type={type}
        value={value}
        onChange={onChange}
      />
      {button}
    </div>
  </div>
)

TextField.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  button: PropTypes.element,
}

TextField.defaultProps = {
  value: '',
  label: '',
  type: 'text',
  button: null,
}

export default TextField
