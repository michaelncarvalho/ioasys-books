import { getFirstWorld } from '../utils/helpers/String'

test('Deve retornar apenas a primeira palavra do texto', () => {
  expect(getFirstWorld('Alessandro da Silva Gonçalves')).toBe('Alessandro')
})
