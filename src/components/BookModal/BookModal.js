import React from 'react'
import PropTypes from 'prop-types'
import { Dialog, Layout } from 'element-react'
import defaultImage from '../../assets/default-book-image.png'
import quotesImage from '../../assets/quotes-image.png'
import './BookModal.scss'

const BookModal = ({ bookData, dialogVisible, onCancel }) => (
  <div className='dialog-container'>
    <Dialog size='small' visible={dialogVisible} onCancel={() => onCancel()}>
      <Dialog.Body>
        <Layout.Row type='flex' className='modal-content'>
          <Layout.Col className='book-col'>
            <img
              src={bookData.imageUrl || defaultImage}
              alt='Imagem com a capa do livro'
              className='book-image-modal'
            />
          </Layout.Col>
          <Layout.Col className='description-col'>
            <Layout.Row className='modal-book-title'>
              {bookData?.title}
            </Layout.Row>
            <Layout.Row className='modal-authors'>
              {bookData?.authors.join(', ')}
            </Layout.Row>
            <div>
              <div className='subtitle-description'>Informações</div>
              <Layout.Row className='info-row'>
                <Layout.Col>Páginas</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.pageCount}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>Editora</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.publisher}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>Publicação</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.published}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>Idioma</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.language}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>Título Original</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.title}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>ISBNN-10</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.isbn10}
                </Layout.Col>
              </Layout.Row>
              <Layout.Row className='info-row'>
                <Layout.Col>ISBNN-13</Layout.Col>
                <Layout.Col className='value-description'>
                  {bookData?.isbn13}
                </Layout.Col>
              </Layout.Row>
            </div>
            <div className='subtitle-description'>Resenha da editora</div>
            <Layout.Row className='book-description'>
              <img
                src={quotesImage}
                alt='Imagem representando citações'
                className='quotes-image'
              />
              {bookData?.description}
            </Layout.Row>
          </Layout.Col>
        </Layout.Row>
      </Dialog.Body>
    </Dialog>
  </div>
)

BookModal.propTypes = {
  dialogVisible: PropTypes.bool,
  onCancel: PropTypes.func,
  bookData: PropTypes.shape({
    title: PropTypes.string,
    authors: PropTypes.arrayOf(PropTypes.string),
    pageCount: PropTypes.number,
    publisher: PropTypes.string,
    imageUrl: PropTypes.string,
    published: PropTypes.number,
    category: PropTypes.string,
    language: PropTypes.string,
    isbn10: PropTypes.string,
    isbn13: PropTypes.string,
    id: PropTypes.string,
    description: PropTypes.string,
  }),
}

BookModal.defaultProps = {
  dialogVisible: false,
  onCancel: PropTypes.func,
  bookData: {},
}

export default BookModal
